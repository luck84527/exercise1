package com.lifex.proficiencyexercise

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Adapter
import androidx.recyclerview.widget.RecyclerView
import com.lifex.proficiencyexercise.adapter.NewsAdapter
import com.lifex.proficiencyexercise.datamanager.NewsDataManager
import com.lifex.proficiencyexercise.model.RowsItem

class MainActivity : AppCompatActivity() {
    private val rvNews: RecyclerView by lazy { findViewById(R.id.rvNews) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadLocalData()

    }

    private fun loadLocalData() {
        val news = NewsDataManager(this).readNewsData()
        news?.let {
            rvNews.adapter = NewsAdapter(this, news.rows)
        }
    }

}