package com.lifex.proficiencyexercise.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lifex.proficiencyexercise.R
import com.lifex.proficiencyexercise.model.RowsItem

class NewsAdapter constructor(val context: Context, private val list: ArrayList<RowsItem>) :
    RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val layout = LayoutInflater.from(context).inflate(R.layout.adapter_news, parent, false)
        return NewsViewHolder(layout)
    }


    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val data = list[position]
        holder.newsData(data)
    }

    override fun getItemCount(): Int {
        return list.size ?: 0
    }

    class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun newsData(newsItem: RowsItem) {
            val tvHeaderTitle: AppCompatTextView by lazy { itemView.findViewById(R.id.tvHeaderTitle) }
            val tvDescription: AppCompatTextView by lazy { itemView.findViewById(R.id.tvDescription) }
            val ivImage: AppCompatImageView by lazy { itemView.findViewById(R.id.ivImage) }
            tvHeaderTitle.text = newsItem.title
            tvDescription.text = newsItem.description
            Glide.with(itemView.context).load(newsItem.imageHref)
                .placeholder(R.drawable.ic_baseline_image_24).into(ivImage)

        }
    }

}